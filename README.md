**IMPORTANT** Read the section about _Translations with Entity Translation_ if you're planning on doing anything multi-lingual.


# Entity Construction Kit (ECK)

The Entity Construction Kit creates entities for you!

## So, How do you use it?

### Entity Type Administration

The main administration page is located at /admin/structure/eck.
Here you will be able to see all of the entities you have created, under the "Overview" tab.
There is, also, an "Add Entity Type" Link. This is were you go to create your entities, simply give a name for your entity, and that is it. ECK creates entities for the purpose of adding fields to them, so it will create a bundle for you automatically. The name of this bundle is the same that the name that you gave your entity type. If you want your first bundle to be called something different just add the bundle name when creating the entity type.
After adding an entity type, you can go back to the overview tab and see a link to you Bundle administration section.

### Bundle Administration

If you click on an Entity Type's name, you will be taken the the Bundle administration page. Here you will see the bundle that was created automatically for you, and will be able to add more bundles for your entity type.
Entity Administration

If you click on a bundle of your entity type, you will be taken to the entity administration table. There you can add fields to your bundle manage the display for the different view modes, and create new entities.
Once an entity is created it will be shown in the overview tab, and you can further view or edit your entity from there.

That is all.

# Translations with Entity Translation

The generic URL structure employed by ECK busts Drupal's _limit_ for path segments that are considered for routing. &lt;sarcasm&gt; It is perfectly reasonable to limit the number of path segments in an URL to 9 - after all: who could _possibly_ **ever** need more &lt;/sarcasm&gt;. Well... ECK does. To get the UI to add translations to work, you need to up the limit defined in `menu.inc:264` as `define('MENU_MAX_PARTS', 9);`. I would recommend 16 for no reason other than I am worried to increase it more since it might cause performance issues.
