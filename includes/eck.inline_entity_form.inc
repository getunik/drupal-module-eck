<?php

/**
 * @file
 * Defines the inline entity form controller for ECK Entities.
 */

class EckInlineEntityFormController extends EntityInlineEntityFormController {

  /**
   * Overrides EntityInlineEntityFormController::labels().
   */
  public function defaultLabels() {
    $entity_info = entity_get_info($this->entityType);

    return array(
      'singular' => $entity_info['label'],
      'plural' => $entity_info['label'],
    );
  }

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   */
  public function entityForm($entity_form, &$form_state) {
    $this->fixMissingParentLanguage($entity_form, $form_state);

    $entity_form = array_merge($entity_form, eck__entity__form($entity_form, $form_state, $entity_form['#entity']));

    // Process multilingual and shared form elements.
    $this->entityFormSharedElements($entity_form);

    unset($entity_form['submit']);
    return $entity_form;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityFormSubmit().
   */
  public function entityFormSubmit(&$entity_form, &$form_state) {
    $this->fixMissingParentLanguage($entity_form, $form_state);
    parent::entityFormSubmit($entity_form, $form_state);
  }

  /**
   * "Fixes" an extremely hard to track issue with ECK and entity_translation where ECK entities
   * that are created via IEF on a node that has not yet been saved will get their language set
   * to LANGUAGE_NONE and store all their field values under 'und'. This little hack forces the
   * ECK entity language to the entity_translation configured default language when an ECK entity
   * is first created and the parent/host entity does not provide a language.
   */
  private function fixMissingParentLanguage(&$entity_form, &$form_state) {
    $entity = $entity_form['#entity'];
    $info = entity_get_info($this->entityType);
    $parent_info = entity_get_info($entity_form['#parent_entity_type']);

    if (!isset($parent_info['entity keys']['language']) || !isset($form_state['values'][$parent_info['entity keys']['language']])) {
      if ($entity_form['#op'] == 'add') {
        $handler = entity_translation_get_handler($this->entityType, $entity);
        $entity->{$info['entity keys']['language']} = $handler->getDefaultLanguage();
        $handler->setFormLanguage($handler->getDefaultLanguage());
      }
    }
  }
}
